package hr.apps.mosaic.example_101;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Stray on 1.10.2017..
 */

public class CustomBroadcastReciever extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		String s = intent.getStringExtra("message");
		Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
	}
}
