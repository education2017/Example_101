package hr.apps.mosaic.example_101;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity implements View.OnClickListener {

	@BindView(R.id.etMyMessage) EditText etMessage;
	@BindView(R.id.bSendBroadcast) Button bSendBroadcast;

	private static final String CUSTOM_FILTER = "hr.ferit.broadcastfirst.MY_CUSTOM_INTENT";
	private static final String KEY_MESSAGE = "message";

	CustomBroadcastReciever mCustomBroadcastReciever = new CustomBroadcastReciever();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(this.mCustomBroadcastReciever, new IntentFilter(CUSTOM_FILTER));
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(this.mCustomBroadcastReciever);
	}

	@OnClick(R.id.bSendBroadcast)
	public void onClick(View v){
		String msg = etMessage.getText().toString();
		Intent i = new Intent();
		i.putExtra(KEY_MESSAGE, msg);
		i.setAction(CUSTOM_FILTER);
		sendBroadcast(i);
	}
}

